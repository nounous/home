#!/usr/bin/python3
import json
import os
import re
import subprocess

import jinja2
import ldap

path = os.path.dirname(os.path.abspath(__file__))

def get_users(basedn, base):
    users_query_id = base.search(basedn, ldap.SCOPE_ONELEVEL, 'objectClass=posixAccount')
    users = base.result(users_query_id)[1]
    return { user['uid'][0].decode('utf-8'): (int(user['uidNumber'][0].decode('utf-8')), int(user['gidNumber'][0].decode('utf-8'))) for _, user in users }

def get_homes(config):
    return set(os.listdir(config['home']['path']))

def set_quota(folder, uid, quota):
    subprocess.run(['/usr/sbin/zfs', 'set' , f'userquota@{uid}={quota}', folder])

#def set_quota(folder, quota):
#    subprocess.run(['setfattr', '-n', 'ceph.quota.max_bytes', '-v', quota, folder)

def create_home(config, user, uid, gid):
    home = os.path.join(config['path'], user)
    if os.path.exists(home):
        raise Exception(f"Home {home} already exists")
    os.mkdir(home, mode=0o701)
    os.chown(home, uid, gid)
    if not os.path.exists(f'{home}/Mail'):
        os.mkdir(f'{home}/Mail', mode=0o700)
        os.chown(f'{home}/Mail', uid, gid)
    if not os.path.exists(f'{home}/OwnCloud'):
        os.mkdir(f'{home}/OwnCloud', mode=0o700)
        os.chown(f'{home}/OwnCloud', uid, gid)
    if 'quota' in config['quota']:
        set_quota(config['path'].lstrip('/'), uid, config['quota'])

def create_maildir(config, user, uid, gid):
    maildir = os.path.join(config['path'], user)
    if os.path.exists(maildir):
        raise Exception(f"Maildir {maildir} already exists")
    os.mkdir(maildir, mode=0o700)
    os.chown(maildir, uid, gid)
    if 'quota' in config['quota']:
        set_quota(config['path'].lstrip('/'), uid, config['quota'])

def ldap_init(config):
    base = ldap.initialize(config['server'])
    base.simple_bind_s(config['binddn'], config['password'])
    return base

if __name__ == '__main__':
    with open(os.path.join(path,"home.json"), 'r') as config_file:
        config = json.load(config_file)
    homes = get_homes(config)
    base = ldap_init(config['ldap'])
    users = get_users(config['ldap']['basedn'], base)
    new_homes = { user: users[user] for user in users if user not in homes }
    for user in new_homes:
        if not re.fullmatch(r'[a-z][a-z0-9-]+', user):
            raise Exception(f'User {user} has an invalid uid !')
        create_home(config['home'], user, *new_homes[user])
        create_maildir(config['mail'], user, *new_homes[user])
